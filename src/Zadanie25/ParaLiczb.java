package Zadanie25;

public class ParaLiczb {
    private Integer a;
    private Integer b;

    public ParaLiczb(Integer a, Integer b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public String toString() {
        return "ParaLiczb{" +
                "a=" + a +
                ", b=" + b + " " + hashCode() +
                '}';
    }
}
