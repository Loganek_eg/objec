package Zadanie25;

import java.util.*;

public class Zadanie25Main {

    //10,12,10,3,4,12,12,300,12,40,55

    public static void main(String[] args) {
        Integer[] tablicaIntow = {10,12,10,3,4,12,12,300,12,40,55};
        HashSet<Integer> hashSetIntow = new HashSet<>();

        for (int i : tablicaIntow) {
            hashSetIntow.add(i);
        }

        System.out.println("przepisanyHashSet: " + hashSetIntow);
        System.out.println("Liczba elementow: " + hashSetIntow.size());

        System.out.println("wypisane foreachem: ");
        for (Integer i : hashSetIntow) {
            System.out.print(i + " ");
        }

        System.out.println("Usuwam 10 i 12...");
        hashSetIntow.remove(10);
        hashSetIntow.remove(12);

        System.out.println("Dodaje liste raz jeszcze");
        List<Integer> list = Arrays.asList(tablicaIntow);
        hashSetIntow.addAll(list);

        System.out.println("hashSet: " + hashSetIntow);
        System.out.println("Liczba elementow: " + hashSetIntow.size());

        System.out.println(containsDuplicates(hashSetIntow));

        Set<ParaLiczb> setPar = new LinkedHashSet<>();

        setPar.add(new ParaLiczb(1,2));
        setPar.add(new ParaLiczb(2,1));
        setPar.add(new ParaLiczb(1,1));
        setPar.add(new ParaLiczb(1,2));

        System.out.println();

        for (ParaLiczb para: setPar) {
            System.out.println(para);
        }

        System.out.println(setPar);

        Set nowySet = createSet(1.0, 2.0, 3.5, 0.1, 1.0);
        System.out.println(nowySet);

    }

   // e. Napisz metodę sprawdzającą, czy w tekście nie powtarzają się litery z wykorzystaniem zbioru. (boolean containDuplicates(String text))

    public static boolean containsDuplicates(HashSet<Integer> setToCheck){
        Set<Integer> singles = new HashSet<>();
        boolean noDups = true;
        for (Integer element : setToCheck) {
            if(singles.contains(element)){
                noDups = false;
            }
            else {
                singles.add(element);
            }
        }
        return noDups;
    }

    public static HashSet<Double> createSet(Double... liczby){
        HashSet<Double> nowySet = new HashSet<>();
        for (Double liczba: liczby) {
            if (nowySet.contains(liczba)){
                throw new IllegalArgumentException();
            }
            nowySet.add(liczba);
        }
        return nowySet;
    }

}
