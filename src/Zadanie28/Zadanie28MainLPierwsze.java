package Zadanie28;

import java.util.*;

public class Zadanie28MainLPierwsze {
    public static void main(String[] args) {
        Map<Integer, Boolean> czyPierwsze30 = new HashMap<>();

        for (int i = 1; i <= 30; i++) {
            Boolean czyPierwsza = true;
            for (int j = 2; j < i ; j++) {
                if (i % j == 0){
                    czyPierwsza = false;
                }
            }
            czyPierwsze30.put(i, czyPierwsza);
        }

        for (Map.Entry element : czyPierwsze30.entrySet()) {
            System.out.print(element.getKey() + ": ");
            if (element.getValue().equals(true)){
                System.out.print("liczba pierwsza.\n");
            }
            else System.out.print("- \n");
        }

        System.out.println();

        wypiszKonkretne(czyPierwsze30, 1, 4, 5, 7, 9, 12);

    }
    public static void wypiszKonkretne (Map<Integer, Boolean> mapa, Integer... liczby){
        for (Integer liczba: liczby) {
            System.out.println(liczba + " pierwsza:" + mapa.get(liczba));
        }
    }
}
