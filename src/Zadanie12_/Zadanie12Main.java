package Zadanie12_;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Zadanie12Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        String inputLine = "";
        List<Figure> listaFigur = new ArrayList<>();

        while (!inputLine.equals("quit")){
            System.out.println("Choose one of the following options to calculate:");
            System.out.println("- area or perimeter");
            System.out.println("- circle (+ r), square (+ a), rectangle (+ a + b)");

            inputLine = skan.nextLine();
            String[] inputArguments = inputLine.split(" ");
            if(inputLine.equals("quit")){
                break;
            }
            else if(inputArguments[0].equals("area")){
                try {
                    if(inputArguments[1].equals("circle")){
                        Circle circle = new Circle(Double.parseDouble(inputArguments[2]));
                        System.out.println(circle.calculateArea());
                    }
                    else if(inputArguments[1].equals("square")){
                        Square square = new Square(Double.parseDouble(inputArguments[2]));
                        System.out.println(square.calculateArea());
                    }
                    else if(inputArguments[1].equals("rectangle")){
                        Rectangle rectangle = new Rectangle(Double.parseDouble(inputArguments[2]), Double.parseDouble(inputArguments[3]));
                        System.out.println(rectangle.calculateArea());
                    }
                    else {
                        System.out.println("Wrong figure.");
                    }

                }catch (NumberFormatException nfe){
                    System.out.println("Incorrect argument format");
                }catch (ArrayIndexOutOfBoundsException aioob){
                    System.out.println("Not enough arguments");
                }
            }
            else if(inputArguments[0].equals("perimeter")){
                try {
                    if(inputArguments[1].equals("circle")){
                        Circle circle = new Circle(Double.parseDouble(inputArguments[2]));
                        System.out.println(circle.calculatePerimeter());
                    }
                    else if(inputArguments[1].equals("square")){
                        Square square = new Square(Double.parseDouble(inputArguments[2]));
                        System.out.println(square.calculatePerimeter());
                    }
                    else if(inputArguments[1].equals("rectangle")){
                        Rectangle rectangle = new Rectangle(Double.parseDouble(inputArguments[2]), Double.parseDouble(inputArguments[3]));
                        System.out.println(rectangle.calculatePerimeter());
                    }
                    else {
                        System.out.println("Wrong figure.");
                    }

                }catch (NumberFormatException nfe){
                    System.out.println("Incorrect argument format");
                }catch (ArrayIndexOutOfBoundsException aioob){
                    System.out.println("Not enough arguments");
                }
            }
            else {
                System.out.println("Wrong command.");
            }
        }
    }
}
