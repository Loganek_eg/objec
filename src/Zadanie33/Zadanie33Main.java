package Zadanie33;

import java.util.HashSet;
import java.util.Set;

public class Zadanie33Main {
    public static void main(String[] args) {
        Cat cat = new Cat();
        Dog dog = new Dog();

        Set<IAnimal> animalSet = new HashSet<>();

        animalSet.add(cat);
        animalSet.add(dog);
        animalSet.add(new Dog());
        animalSet.add(new Cat());

        for (IAnimal animal : animalSet) {
            animal.makeNoise();
        }
    }
}
