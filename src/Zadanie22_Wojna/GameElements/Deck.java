package Zadanie22_Wojna.GameElements;

import java.util.ArrayList;

public class Deck2 {

    public ArrayList<Card> cardList = fillDeck();

    private ArrayList<Card> fillDeck() {
        ArrayList<Card> tempCardList = new ArrayList<>();
        for (int i = 0; i < 13; i++) {
            for (Suit suit : Suit.values()) {
                tempCardList.add(new Card(i, suit));
            }
        }
        return tempCardList;

    }


}
