package Zadanie22_Wojna;


public class Card {
    private int value; // 1 :13
    private Suit suit; //club, diamond, heart, spade

    public Card(int value, Suit suit) {
        this.value = value;
        this.suit = suit;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "|" + value +
                ", " + suit +
                '|';
    }
}
