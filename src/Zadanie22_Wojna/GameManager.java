package Zadanie22_Wojna;

import Zadanie22_Wojna.GameElements.Card;
import Zadanie22_Wojna.GameElements.Deck;
import Zadanie22_Wojna.GameElements.Player;
import Zadanie22_Wojna.GameElements.Table;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Random;

public class Zadanie22Main {


    //zrobic licznik czasu gry


    public static void main(String[] args) {
        GameStats gameStats = new GameStats();
        Player player1 = new Player("Stefan");
        Player player2 = new Player("Joozek");

        while (true) {
            GameResults gameResults = new GameResults();

            gameResults.start = LocalDateTime.now();
            Deck deck = new Deck();


            // rozlosuj karty
            dealCardsToPlayers(player1, player2, deck);
            //wypisz karty graczy
            printPlayersCards(player1, player2);
            //jezeli obaj gracze maja karty
            while (player1.playerCardList.size() > 0 && player2.playerCardList.size() > 0) {
                Table turnTable = new Table();

                //dodaj pierwsze karty playerow do ich stosow na stole
                makeTurn(false, player1, player2, turnTable);
                //zbierz wartosci gornych kart na stole
                updatePlayerCurrentCardValue(player1, player2, turnTable);
                //jezeli nie ma wojny
                if (!war(player1, player2)) {
                    try {
                        makeTurn(true, player1, player2, turnTable);
                    } catch (IndexOutOfBoundsException ioobe) {
                        gameResults.stageCount++;
                        break;
                    }
                    Player winner = getStageWinner(player1, player2);
                    shuffleReturn(winner, turnTable);
                    gameResults.stageCount++;
                }
                //jezeli jest wojna
                else {
                    //jesli jest wojna
                    while (war(player1, player2)) {
                        try {
                            makeTurn(true, player1, player2, turnTable);
                        } catch (IndexOutOfBoundsException ioobe) {
                            gameResults.stageCount++;
                            break;
                        }
                        updatePlayerCurrentCardValue(player1, player2, turnTable);
                        gameResults.stageCount++;
                    }
                    gameResults.warCount++;
                    //Sprawdz kto wygral i losowo dodaj mu karty do jego talii
                    Player winner = getStageWinner(player1, player2);
                    shuffleReturn(winner, turnTable);
                }

                System.out.println("                 " + player1.currentCardValue + " vs " + player2.currentCardValue);
                System.out.println("StageCount: " + gameResults.stageCount);
                System.out.println("WarCount: " + gameResults.warCount);
                System.out.println(player1.playerCardList.size() + " vs " + player2.playerCardList.size() + "cards");


            }
            System.out.println("<<<<<<<<<<<<<<<<<<<<");
            gameResults.finish = LocalDateTime.now();
            gameResults.updateDuration();

            if (player1.playerCardList.size() == 0) {
                player2.winner = true;
                gameResults.winner = player2;
                player2.gamesWonCounter++;
            } else{
                player1.winner = true;
                gameResults.winner = player1;
                player1.gamesWonCounter++;
            }
            printResults(gameResults);
            gameStats.updateStats(gameResults);
            gameStats.printStats();
            player1.clearDeck();
            player2.clearDeck();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static void updateGameStats(GameResults gameResults) {


    }

    private static boolean war(Player player1, Player player2) {
        return player1.currentCardValue == player2.currentCardValue;
    }

    private static void updatePlayerCurrentCardValue(Player player1, Player player2, Table turnTable) {
        player1.currentCardValue = turnTable.player1Stack.get(0).getValue();
        player2.currentCardValue = turnTable.player2Stack.get(0).getValue();
    }

    private static Player getStageWinner(Player player1, Player player2) {
        Player winner;
        if (player1.currentCardValue > player2.currentCardValue) winner = player1;
        else winner = player2;
        return winner;
    }

    private static void printPlayersCards(Player player1, Player player2) {
        System.out.println(player1.playerCardList);
        System.out.println(player2.playerCardList);
    }

    private static void dealCardsToPlayers(Player player1, Player player2, Deck deck) {
        Random random = new Random();
        while (deck.cardList.size() > 0) {
            int randomIndex = random.nextInt(deck.cardList.size());
            player1.playerCardList.add(deck.cardList.get(randomIndex));
            deck.cardList.remove(randomIndex);
            randomIndex = random.nextInt(deck.cardList.size());
            player2.playerCardList.add(deck.cardList.get(randomIndex));
            deck.cardList.remove(randomIndex);
        }
    }

    public static void shuffleReturn(Player winner, Table table) {
        Random random = new Random();
        ArrayList<Card> tableCards = new ArrayList<>();
        tableCards.addAll(table.player1Stack);
        tableCards.addAll(table.player2Stack);

        while (!tableCards.isEmpty()) {
            int randomCardIndex = random.nextInt(tableCards.size());
            winner.playerCardList.add(tableCards.get(randomCardIndex));
            tableCards.remove(randomCardIndex);
        }


    }

    public static void makeTurn(boolean war, Player player1, Player player2, Table turnTable) {
        if (war) {
            //przenies po dwie karty od playerow na gore ich stosow na stole
            turnTable.player1Stack.add(0, player1.playerCardList.get(0));
            turnTable.player1Stack.add(0, player1.playerCardList.get(1));
            player1.playerCardList.remove(0);
            player1.playerCardList.remove(0);
            turnTable.player2Stack.add(0, player2.playerCardList.get(0));
            turnTable.player2Stack.add(0, player2.playerCardList.get(1));
            player2.playerCardList.remove(0);
            player2.playerCardList.remove(0);
        } else {
            ////przenies po jednej karcie od playerow na gore ich stosow na stole
            turnTable.player1Stack.add(0, player1.playerCardList.get(0));
            turnTable.player2Stack.add(0, player2.playerCardList.get(0));
            player1.playerCardList.remove(0);
            player2.playerCardList.remove(0);
        }
        //zbierz wartosci gornych kart
        updatePlayerCurrentCardValue(player1, player2, turnTable);

    }

    public static void printResults(GameResults gameResults) {
        System.out.println("Player " + gameResults.winner.getName()+ " won in " + gameResults.stageCount + " turns.");
        System.out.println(gameResults.warCount + " wars took place.");
        System.out.println("Game duration millis: " + gameResults.duration);
        System.out.println();
    }


    public static void historicalShuffleReturn2(ArrayList<Card> winnersList, Table table) {
        Random random = new Random();

        ArrayList<Card> tableStack1 = table.player1Stack;
        ArrayList<Card> tableStack2 = table.player2Stack;

        while (!tableStack1.isEmpty() || !tableStack2.isEmpty()) {
            if (!tableStack1.isEmpty() && !tableStack2.isEmpty()) {
                int stackChoice = random.nextInt(2);
                System.out.println("Stackchoice: " + stackChoice);
                switch (stackChoice) {
                    case 0: {
                        int randomCardIndex = random.nextInt(tableStack1.size());
                        Card randomCard = tableStack1.get(randomCardIndex);
                        winnersList.add(randomCard);
                        tableStack1.remove(randomCardIndex);
                        break;
                    }
                    case 1: {
                        int randomCardIndex = random.nextInt(tableStack2.size());
                        Card randomCard = tableStack2.get(randomCardIndex);
                        winnersList.add(randomCard);
                        tableStack2.remove(randomCardIndex);
                        break;
                    }
                    default:
                        break;
                }
                break;
            } else if (!tableStack1.isEmpty() && tableStack1.isEmpty()) {
                int randomCardIndex = random.nextInt(tableStack1.size());
                Card randomCard = tableStack1.get(randomCardIndex);
                winnersList.add(randomCard);
                tableStack1.remove(randomCardIndex);
            } else if (!tableStack2.isEmpty() && tableStack1.isEmpty()) {
                int randomCardIndex = random.nextInt(tableStack2.size());
                Card randomCard = tableStack2.get(random.nextInt(tableStack2.size()));
                winnersList.add(randomCard);
                tableStack2.remove(randomCardIndex);
            }

        }
        System.out.println("koniec");

    }

}
