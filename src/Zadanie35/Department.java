package Zadanie35;

import java.util.ArrayList;
import java.util.HashMap;

public class Department {
    private HashMap<String, Office> officeMap = new HashMap<>();

    public void addOffice(String city){
        officeMap.put(city, new Office(city));
    }

    public Office getOffice (String city){
        return officeMap.get(city);
    }
}
