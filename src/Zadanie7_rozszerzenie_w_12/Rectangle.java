package Zadanie7_rozszerzenie_w_12;

public class Rectangle {
    private double legA;
    private double legB;

    public double calculatePerimeter(){
        return 2*legA + 2* legB;
    }
    public double calculateArea(){
        return legA * legB;
    }

    public void setLegA(double legA) {
        this.legA = legA;
    }

    public void setLegB(double legB) {
        this.legB = legB;
    }
}
