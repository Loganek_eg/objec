package Zadanie27;

import Zadanie22_Wojna.Deck;
import Zadanie26.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Zadanie27MainBCD {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        System.out.println("Wpisz linie tekstu");
        String linia = skan.nextLine();

        String [] tablica = linia.split(" ");
        List<String> lista = new ArrayList<>();

        for (String slowo : tablica) {
            lista.add(slowo);
        }

        System.out.println("Liczba slow: " + lista.size());

        int iloscZnakow = 0;

        for (String slowo: lista) {
            iloscZnakow += slowo.toCharArray().length;
        }

        System.out.println("Ilosc znakow w slowach to: " + iloscZnakow);

        Double srednia = (double) iloscZnakow / (double)lista.size();
        System.out.println("Srednia ilosc znakow w slowie to: " + srednia);

        System.out.println(nowaLista(new Student(13424L, "asdf", "asdf"), new Deck(), 3, "HelloWorld"));
    }

    public static <T extends Object> ArrayList<T> nowaLista(T... elements){
        ArrayList<T> lista = new ArrayList<>();
        for (T element : elements) {
            lista.add(element);
        }
        return lista;
    }
}
