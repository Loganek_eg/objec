package Zadanie21;

import java.util.ArrayList;
import java.util.List;

public class Town {
    List<Citizen> citizenList = new ArrayList<>();

    public void addCitizen(Citizen citizenToAdd){
        citizenList.add(citizenToAdd);
    }

    //howManyCanVote
    public int howManyCanVote(){
        int howMany = 0;
        for (Citizen cit : citizenList) {
            if(cit.canVote()){
                howMany++;
            }
        }
        return howMany;
    }

    //whoCanVote
    public ArrayList<String> whoCanVote(){
        ArrayList<String> whoCan = new ArrayList<>();
        for (Citizen cit : citizenList) {
            if(cit.canVote()){
                whoCan.add(cit.getName());
            }
        }
        return whoCan;
    }
}
