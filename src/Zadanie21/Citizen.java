package Zadanie21;

public abstract class Citizen {
    private String name;
    protected abstract boolean canVote();

    public Citizen(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
