package Zadanie6;

public class QuadraticEquation {
    private double a;
    private double b;
    private double c;


    public double calculateDelta(){
        double delta = Math.pow(b, 2) - 4*a*c;
        return delta;
    }
    public double calculateX1(){
        double x1 = (-1*b - Math.sqrt(calculateDelta()))/2*a;
        return x1;
    }
    public double calculateX2(){
        double x2 = (-1*b + Math.sqrt(calculateDelta()))/2*a;
        return x2;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setC(double c) {
        this.c = c;
    }
}
