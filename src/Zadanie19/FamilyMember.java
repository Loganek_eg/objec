package Zadanie19;

public abstract class FamilyMember {
    private String name;

    public FamilyMember(String name) {
        this.name = name;
    }

    public void introduce(){
        System.out.println("I am a " + getClass());
    }

    public abstract boolean isAdult();
}
