package Zadanie19;

import java.util.ArrayList;
import java.util.List;

public class Zadanie19Main {
    public static void main(String[] args) {
        // FamilyMember first = new FamilyMember("Nieokreslony");
        // first.introduce();

        Mother mother1 = new Mother("Katarzyna");
        mother1.introduce();

        Father father1 = new Father("Krzysztof");
        Daughter daughter1 = new Daughter("Julia");
        Son son1 = new Son("Wiktor");

        List<FamilyMember> family1 = new ArrayList<>();
        family1.add(mother1);
        family1.add(father1);
        family1.add(daughter1);
        family1.add(son1);

        System.out.println();

        for (FamilyMember member: family1) {
            member.introduce();
        }
    }
}
