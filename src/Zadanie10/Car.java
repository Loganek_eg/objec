package Zadanie10;

public class Car {
    private String Make = "Ford Escort Cosworth";
    private int seatAmount = 5;
    private int engineCapacity = 2000;
    private int enginePower = 140;
    private int currentSpeed = 0;
    private int mileage = 342578;
    private int modelYear = 1992;
    private int passengerAmount = 0;
    private int maxSpeed = calculateMaxSpeed(enginePower);

    private int calculateMaxSpeed(int enginePower){
        if(enginePower <= 200){
            return (int)(enginePower * 1.2);
        }
        else {
            return enginePower;
        }
    }


    public String getCarName (){
        return (Make + ", " + modelYear);
    }
    public void addPassenger(){
        if(currentSpeed !=0){
            System.out.println("Stop the car first!");
        }
        else if(passengerAmount < seatAmount){
            passengerAmount++;
        }
        else {
            System.out.println("You've reached maximum passenger capacity (" + seatAmount + ").");
        }
    }
    public void removePassenger(){
        if(currentSpeed != 0){
            System.out.println("Stop the car first!");
        }
        else if (passengerAmount != 0){
            passengerAmount--;
        }
        else {
            System.out.println("There are currently no passengers in the car to remove.");
        }
    }
    public void speedIncrease(int acceleration) {
        if (passengerAmount >= 1 && currentSpeed < maxSpeed) {
            if (acceleration < 0) {
                speedDecrease(Math.abs(acceleration));
            } else if (maxSpeed - acceleration > 0) {
                currentSpeed += acceleration;
            } else if (maxSpeed - acceleration <= 0) {
                currentSpeed = maxSpeed;
                System.out.println("You've reached the maximum speed (" + maxSpeed + ").");
            }

        }
        else if(passengerAmount == 0){
            System.out.println("You cannot accelerate with nobody driving!");
        }
    }
    public void speedDecrease(int deceleration){
        if (passengerAmount >= 1 && currentSpeed != 0){
            if(deceleration < 0){
                speedIncrease(Math.abs(deceleration));
            }
            else if (currentSpeed - deceleration > 0){
                currentSpeed -= deceleration;
            }
            else if (currentSpeed - deceleration <= 0){
                currentSpeed = 0;
                System.out.println("The car has stopped.");
            }
        }
        else if (passengerAmount < 1){
            System.out.println("There is no driver to slow the car down!");
        }
        else {
            System.out.println("The car is stationary.");
        }
    }


    @Override
    public String toString() {
        return "Car{" +
                "Make='" + Make + '\'' +
                ", seatAmount=" + seatAmount +
                ", engineCapacity=" + engineCapacity +
                ", enginePower=" + enginePower +
                ", currentSpeed=" + currentSpeed +
                ", mileage=" + mileage +
                ", modelYear=" + modelYear +
                ", passengerAmount=" + passengerAmount +
                ", maxSpeed=" + maxSpeed +
                '}';
    }
}
