package Zadanie9;

import java.util.Scanner;

public class Zadanie9Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        MyMath myMath = new MyMath();
        System.out.println("Enter two numbers: ");
        int i1, i2;
        double d1, d2;
        try {
            String line = skan.nextLine();
            i1 = Integer.parseInt(line.split(" ", 2)[0]);
            i2 = Integer.parseInt(line.split(" ", 2)[1]);
            d1 = Double.parseDouble(line.split(" ", 2)[0]);
            d2 = Double.parseDouble(line.split(" ", 2)[1]);

            System.out.println("Absolute value of 1st number in int: " +myMath.abs(i1));
            System.out.println("Absolute value of 2nd number in int: " +myMath.abs(i2));
            System.out.println("Absolute value of 1st number in double : " +myMath.abs(d1));
            System.out.println("Absolute value of 2nd number in double : " +myMath.abs(d2));
            System.out.println("First number to the power of second (int): " + myMath.pow(i1, i2));
            System.out.println("First number to the power of second (double): " + myMath.pow(d1, d2));
        }
        catch (NumberFormatException nfe){
            System.out.println("Incorrect number format.");
        }

    }
}
