package Zadanie26;

import java.util.HashMap;
import java.util.Map;

public class Zadanie26Main {
    public static void main(String[] args) {
        HashMap<Integer, Student> mapaStudentow = new HashMap<>();

        mapaStudentow.put(1, new Student(100400, "Krzysztof", "Topolski"));
        mapaStudentow.put(2, new Student(102342, "Tomasz", "Grzeszczak"));
        mapaStudentow.put(3, new Student(123423, "Aleksander", "Doba"));
        mapaStudentow.put(4, new Student(100500, "Agata", "Biziuk"));

        System.out.println("Czy zawiera indeks 100400: " + czyZawieraNumerIndeksu(mapaStudentow, 100400));
        System.out.println("Czy zawiera indeks 100200: " + czyZawieraNumerIndeksu(mapaStudentow, 100200));


        // wypisz studenta z indeksem 100400
        for (Student student : mapaStudentow.values()) {
            if(student.getNumerIndeksu() == 100400){
                System.out.println(student);
            }
        }

        //wypisz liczbe studentow
        System.out.println(mapaStudentow.size() + " to liczba studentow.");
        
        //wypisz wszystkich studentow
        for (Map.Entry<Integer, Student> daneOStudencie: mapaStudentow.entrySet()){
            System.out.println(daneOStudencie.getValue());
        }

        University uni  = new University();

        uni.addNewStudent(5L, "Krzysztof", "Kobiela");
        uni.addNewStudent(3L, "Krzysztof", "Wyklety");
        uni.addNewStudent(2L, "Damian", "Olewczyk");
        uni.addNewStudent(4L, "Ryszard", "Tomczyk");
        uni.addNewStudent(1L, "Leon", "Garczynski");

        System.out.println("Czy zawiera studenta o indeksie 6: " + uni.containsStudent(6L));

        uni.printAllStudents();

        System.out.println(uni.getStudent(5L));

        System.out.println("Ilosc studentow na uni: " + uni.studentsCount());




    }
    public static boolean czyZawieraNumerIndeksu(HashMap<Integer, Student> mapa, long numerIndeksu){
        boolean zawiera = false;
        for (Student wartosc : mapa.values()) {
            if(wartosc.getNumerIndeksu() == numerIndeksu){
                zawiera = true;
            }
        }
        return zawiera;
    }
}
