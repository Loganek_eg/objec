package Zadanie31;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Zadanie31Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        String stryng = "";
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy, HH:mm:ss.SSS");


        do{
            System.out.println("Wcisnij enter to get czas or quit aby exit: ");
            stryng = skan.nextLine();
            if (stryng.equals("quit")){
                break;
            }
            else {
                System.out.println(LocalDateTime.now().format(dtf));
            }

        }while (!stryng.equals("quit"));
    }
}
