package Zadanie34;

import java.util.Scanner;

public class Zadanie34Main {
    public static void main(String[] args) {

        Scanner skan = new Scanner(System.in);
        String inputLine = "";

        while (!inputLine.equals("quit")){
            System.out.println("Wpisz Enuma: KOBIETA / MEZCZYZNA");
            inputLine = skan.nextLine();
            try {
                Plec plecWpisana = Plec.valueOf(inputLine.trim());
                System.out.println("Wpisales: " + plecWpisana);
            }catch (Exception e){
                System.out.println(e);
            }
        }

    }
}
